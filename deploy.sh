#!/bin/bash

set -e

remote="santatra@185.246.87.101"
artifact="demoCICD-*.war"

echo "Building ..."
./mvnw clean install
echo "Copying to remote ..."
rsync -azP target/$artifact $remote:/opt/tomcat/webapps/demoCICD.war
